ARG imgsize=1024
FROM debian:stretch

WORKDIR /root

#prepare build env
RUN dpkg --add-architecture armhf
RUN apt-get update
RUN apt-get -y install curl build-essential git crossbuild-essential-armhf debootstrap multistrap device-tree-compiler bc u-boot-tools mtools parted e2fslibs e2fslibs-dev dosfstools

# prepare sources before building
ENV LINUX_VERSION=4.14.50
RUN curl -O https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-${LINUX_VERSION}.tar.xz
RUN git clone https://github.com/oskarirauta/populatefs.git
RUN git clone -b a13brd --depth=1 --single-branch https://bitbucket.org/savant2212/u-boot-a13.git

RUN mkdir /out/

ENV CROSS=arm-linux-gnueabihf-
ENV BOARD=a13brd
ENV IMGOUT=/out/sdcard.img

# prepare populatefs
RUN make -C populatefs install-bin

ADD files/u-boot/. /configs/u-boot/
# compile u-boot
RUN cp /configs/u-boot/.config ./u-boot-a13; cp /configs/u-boot/a13brd.dts ./u-boot-a13/arch/arm/dts/
RUN make -C u-boot-a13 -j $(nproc) ARCH=arm DEVICE_TREE=${BOARD} CROSS_COMPILE=${CROSS} all dtbs
RUN cp u-boot-a13/SPL u-boot-a13/u-boot.img u-boot-a13/arch/arm/dts/a13brd.dtb u-boot-a13/u-boot-dtb.img /out

ADD files/linux/. /configs/linux/

RUN curl -O http://cdimage.ubuntu.com/ubuntu-base/releases/18.04/release/ubuntu-base-18.04-base-armhf.tar.gz
RUN mkdir /out/rootfs

RUN tar xf  ubuntu-base-18.04-base-armhf.tar.gz -C /out/rootfs

# append custom files to rootfs
ADD files/extra/. /out/rootfs

# compile kernel

RUN tar xf linux-${LINUX_VERSION}.tar.xz
RUN cp /configs/linux/.config ./linux-${LINUX_VERSION}
RUN make -C linux-${LINUX_VERSION} -j $(nproc) ARCH=arm DEVICE_TREE=${BOARD} LOADADDR=0x80008000 CROSS_COMPILE=${CROSS} uImage
RUN make -C linux-${LINUX_VERSION} ARCH=arm -j $(nproc) INSTALL_MOD_PATH=/out/rootfs/  CROSS_COMPILE=${CROSS} modules
RUN make -C linux-${LINUX_VERSION} ARCH=arm INSTALL_MOD_PATH=/out/rootfs/ modules_install
#RUN make -C linux-${LINUX_VERSION} ARCH=arm INSTALL_MOD_PATH=/out/rootfs/ firmware_install
RUN cp linux-${LINUX_VERSION}/arch/arm/boot/uImage /out

# run multistrap to get rootfs

ARG imgsize

#prepare sparse image files
RUN (dd if=/dev/zero of=boot.img bs=1MiB count=0 seek=100; dd if=/dev/zero of=rootfs.img bs=1MiB count=0 seek=512; dd if=/dev/zero of=${IMGOUT} bs=1MiB count=0 seek=$imgsize)

# prepare partition map on output image
RUN parted --script ${IMGOUT} mklabel msdos mkpart primary 2MiB 100MiB mkpart primary 100MiB 612MiB mkpart primary 612MiB 100%

# prepare fs for vfat partition
RUN mkfs.vfat boot.img

# copy files to vfat partition
RUN mcopy -i boot.img /out/a13brd.dtb /out/u-boot.img /out/u-boot-dtb.img /out/uImage ::/

# generate ext4 on image
RUN mkfs.ext4 -F -E root_owner=0:0 -m 0 rootfs.img

# execute multistrap to create file system
#ADD files/multistrap/. /configs/multistrap/
#RUN multistrap -a armhf -d /out/rootfs -f /configs/multistrap/multistrap.conf

#RUN debootstrap \
#	--arch=armhf \
#	--keyring=/usr/share/keyrings/ubuntu-archive-keyring.gpg \
#	--verbose \
#	--foreign \
#	bionic \
#	/out/rootfs

RUN rm /out/rootfs/.gitempty

ADD files/qemu-arm-static /out

# prepare customize script
ADD files/setup.sh /

RUN chmod +x /setup.sh

# rootfs prepared. set entrypoint

ENTRYPOINT /setup.sh


