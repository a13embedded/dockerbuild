#!/bin/sh
rm -f sdcard.img
docker build . --tag a13brd_build
if [ "$?" = "0" ]; then
	docker run --privileged --rm -v $(pwd):/mnt a13brd_build
fi
