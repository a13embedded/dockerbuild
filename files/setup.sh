#!/bin/sh

#apt-get -y install qemu-user-static

cp /out/qemu-arm-static /out/rootfs/usr/bin/qemu-arm-static

mount -t proc /proc /out/rootfs/proc
mount -t sysfs /sys /out/rootfs/sys
mount -o bind /dev /out/rootfs/dev
mount -o bind /dev/pts /out/rootfs/dev/pts

cat <<EOF > /out/rootfs/script.sh
export PATH=/bin:/sbin:/usr/bin:/usr/sbin
echo nameserver 8.8.8.8 > /etc/resolv.conf

apt-get update
apt-get -y --no-install-recommends install systemd systemd-sysv iproute2 iptables bash-completion netbase net-tools ethtool nano iputils-ping kmod

echo root:root | chpasswd

systemctl enable getty@ttymxc0.service

exit
EOF
chmod +x /out/rootfs/script.sh

chroot /out/rootfs  /bin/bash /script.sh

rm /out/rootfs/usr/bin/qemu-arm-static
rm /out/rootfs/script.sh

umount /out/rootfs/dev/pts
umount /out/rootfs/dev
umount /out/rootfs/sys
umount /out/rootfs/proc

# populate rootfs image from ./rootfs directory
populatefs -U -d /out/rootfs rootfs.img

# write prepared images to out.img in sequence
(dd if=/out/SPL of=${IMGOUT} bs=1K seek=1 conv=notrunc;dd if=boot.img of=${IMGOUT} bs=1MiB seek=2 conv=notrunc; dd if=rootfs.img of=${IMGOUT} bs=512 seek=204800)

cp /out/sdcard.img /mnt/sdcard.img
