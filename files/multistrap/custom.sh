#!/bin/sh

set -e -x

# Emdebian grip compatibility version

#pass path to the root. Don't let it run without one as it will break your system
if [ "" = "$1" ] ; then 
  echo "You need to specify a path to the target rootfs"
else
  if [ -e "$1" ] ; then
    ROOTFS="$1"
  else 
    echo "Root dir $ROOTFS not found"
  fi
fi

if [ "/" = "$ROOTFS" ] ; then echo "Refusing to change your build system's files" ; fi

#Do things that need to be done at 1st stage so that rootfs will boot.

#Set securetty
#Add modules - e.g. to support USB serial/ethernet console

# specify config to use
CONFIG="/configs/multistrap/files"

CONSOLE=ttymxc0
   #add serial ports to securetty - now idempotent
DONECONSOLE=`grep $CONSOLE $ROOTFS/etc/securetty || true`
if [ -z "$DONECONSOLE" ]; then
  echo "$CONSOLE" >> $ROOTFS/etc/securetty
fi
DONETELNET=`grep pts/0 $ROOTFS/etc/securetty || true`
if [ -z "$DONETELNET" ]; then
  echo "pts/0" >> $ROOTFS/etc/securetty
fi
   #put our standard fstab and network and modules files in
if [ ! -d $ROOTFS/etc/network ]; then mkdir -p $ROOTFS/etc/network; fi
if [ ! -d $ROOTFS/etc/init.d ]; then mkdir -p $ROOTFS/etc/init.d; fi
if [ ! -d $ROOTFS/etc/dhcp3 ]; then mkdir -p $ROOTFS/etc/dhcp3; fi
if [ ! -d $ROOTFS/etc/network ]; then mkdir -p $ROOTFS/etc/network; fi

if [ ! -d $ROOTFS/etc/apt/apt.conf.d/ ]; then 
  mkdir -p $ROOTFS/etc/apt/apt.conf.d/
fi
cp -v $CONFIG/fstab $ROOTFS/etc/fstab
cp -v $CONFIG/interfaces $ROOTFS/etc/network/interfaces
cp -v $CONFIG/modules $ROOTFS/etc/modules

#mkdir -p  /etc/systemd/system/getty.target.wants/

#ln -s /etc/systemd/system/getty@.service /etc/systemd/system/getty.target.wants/getty@ttymxc0.service

#cp -v $CONFIG/dhclient.conf $ROOTFS/etc/dhcp3/
#cp -v $CONFIG/urandom $ROOTFS/etc/init.d/
#cp -v $CONFIG/inittab $ROOTFS/etc/
#cp -v $CONFIG/10disablerecommends $ROOTFS/etc/apt/apt.conf.d/
   # make sure sudo uses group sudo by default
#rm -f $ROOTFS/etc/sudoers
#cp -v $CONFIG/sudoers $ROOTFS/etc/
#chmod 400 $ROOTFS/etc/sudoers
   # creating devices
#set hostname
echo px-imx6-v2 > $ROOTFS/etc/hostname
#be nice to put these in the right places in files (perl -pi?)
echo "127.0.0.1       localhost.localdomain   localhost" > ${ROOTFS}/etc/hosts
